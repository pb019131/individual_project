import numpy as np
from moviepy.audio.tools.cuts import find_audio_period
from moviepy.editor import *
from moviepy.video import fx
from moviepy.video.tools.cuts import find_video_period
from audio_analyser import AudioAnalyser
from chunk_manager import ChunkManager
from processing import Processing
import multiprocessing
import cv2 as cv

class TimelineManager():
    # variables
    BPM = 0
    duration = 0.0
    transients = None
    key = ""
    scale = ""
    timeline, videos = [], []
    # audio = None
    c_manager = None
    chunk_length = 0.0
    chunk_number = 0
    chunk_weight = 1.0
    finalComposition = VideoClip()

    def __init__(self, video_loc, audio_loc, chunk_weight, layers, debug, transient, load, save, transparent):
        self.audio = AudioFileClip(audio_loc)
        self.audio_loc = audio_loc
        self.duration = self.audio.duration
        self.videos = list(video_loc)
        self.chunk_weight = chunk_weight
        # Call initial audio analysis and set timeline params
        self.a_analyser = AudioAnalyser(audio_loc, self.duration)
        bundle = self.a_analyser.get_params()
        self.debug = debug
        self.transient = transient
        self.set_params(bundle[0], bundle[1], bundle[2])
        self.layers = layers
        self.tracks = []
        self.transient_length = 0
        self.load = load
        self.save = save
        self.transparent = transparent
        self.audio_period = find_audio_period(self.audio)
        print('Analyzed the audio, found a period of %.02f seconds' % self.audio_period)

    def set_params(self, BPM, key, transients):
        self.BPM = BPM
        self.key = key
        if self.transient:
            self.transients = transients
        else:
            self.transients = []
        self.chunk_length = (60 / self.BPM) * 0.5 * self.chunk_weight
        self.chunk_number = int(round(self.get_duration() / self.chunk_length))
        self.c_manager = ChunkManager(self.a_analyser, self.duration, self.chunk_number, self.chunk_length, self.videos)

    def get_duration(self):
        return self.duration

    def get_chunks(self, accuracy, transient_w):
        self.c_manager.get_audio_chunks(accuracy)
        print("Required chunk number: ", self.chunk_number)
        print("Chunk length: ", self.chunk_length)
        if self.transient:
            self.transient_length = self.chunk_length * (0.01 * transient_w)
            print("Transient Length:", self.transient_length)
            self.c_manager.get_audio_transient_chunks(accuracy)

        # If enabled load relavant chunks, if any
        if self.load:
            self.c_manager.load_chunk_data()
        self.c_manager.divide_video_chunks(accuracy)
        parallel = Processing(function=self.c_manager.prepare_video_chunk, data=self.c_manager.starts)
        parallel.run()

        # SAVE CHUNKS (overwrites existing files)
        if self.save:
            self.c_manager.save_chunk_data()

        if len(self.c_manager.video_chunks) == 0 or len(self.c_manager.audio_chunks) == 0:
            print("Error encoutered, couldn't extract chunks, the timings might be out of range.")
            return
        else:
            matches = self.c_manager.match_chunks(self.debug)
            order = 0

            # repeat and copy layers that need to be composed later
            for x in range(self.layers):
                for m in matches:
                    self.add_chunk(m, order)
                    order += 1
                if x == 0:
                    self.tracks.append(self.collect_timeline_items(base=True))
                    self.timeline = []
                    if self.transient:
                        matches = self.c_manager.transient_best_fit
                else:
                    self.tracks.append(self.collect_timeline_items(base=False))
                    self.timeline = []
            self.videos = list(self.videos)

    def add_chunk(self, chunk_num, order):
        chunk = self.c_manager.video_chunks[chunk_num]
        if chunk.end <= self.c_manager.videos[chunk.file].duration:
            # forced resolution 1080p
            # , target_resolution = (1080, 1920)
            # timeline_mask = timeline_mask.resize(width=1920)

            # add the matched chunk
            timeline_chunk = VideoFileClip(self.videos[chunk.file]).subclip(chunk.start, chunk.end)
            update_msg = str(order+1) + "/" + str(self.chunk_number + len(self.transients)) + " - Adding chunk from: " + str(timeline_chunk.filename)
            print(update_msg)

            # generate the moving mask
            if self.transparent:
                def make_mask_frame(frame):
                    hue = self.c_manager.v_analyser.colours(frame)
                    mask_frame = chunk.chroma_key(frame, hue)
                    return mask_frame

                mask_vid = timeline_chunk.fl_image(make_mask_frame)
                timeline_mask = mask_vid.set_ismask(True)
                timeline_mask = timeline_mask.set_position("center")

                # some_background = VideoFileClip('media/light.mov', audio=False, target_resolution=(timeline_chunk.h, timeline_chunk.w)).set_position("center")
                # some_background = some_background.speedx(final_duration=self.audio_period/2).fx(vfx.loop, duration=self.duration)
                # some_background,
                timeline_chunk = timeline_chunk.set_mask(timeline_mask)
                # timeline_chunk = CompositeVideoClip([timeline_chunk.set_mask(timeline_mask)]).set_duration(timeline_chunk.duration).set_position("center")

            if self.debug:
                screensize = timeline_chunk.size
                v_text_chunk = TextClip(chunk.debug_txt, color='yellow', font="Amiri-Bold", align="West", kerning=5,
                                        fontsize=40).set_duration(timeline_chunk.duration)
                a_text_chunk = TextClip(self.c_manager.audio_debug_text[order], color='green', font="Amiri-Bold",
                                        align="West", kerning=5, fontsize=40).set_duration(timeline_chunk.duration)
                timeline_chunk = CompositeVideoClip([timeline_chunk, v_text_chunk.set_position((0.7, 0.4), relative=True), a_text_chunk.set_position((0.7, 0.05), relative=True)], size=screensize).set_duration(timeline_chunk.duration)
                v_text_chunk.close()
                a_text_chunk.close()
            self.timeline.append(timeline_chunk)
            timeline_chunk.close()

    # collect subclips from all of the referenced chunks into VideoClips to the timeline layer and save
    def collect_timeline_items(self, base=True):
        if base:
            timeline = concatenate_videoclips(self.timeline, method="chain").set_duration(self.duration)
        else:
            # timeline = concatenate_videoclips(self.timeline, method="compose").set_duration(self.duration).set_position("center")
            # timeline = concatenate_videoclips(self.timeline).set_duration(self.duration)
            timeline = CompositeVideoClip(self.timeline, size=(1000, 1000)).set_position("center", relative=True)
            timeline = timeline.fx(fx.all.time_mirror)
        if self.layers >= len(self.tracks) and self.transient:
            for count in range(len(self.timeline)):
                self.timeline[count] = self.timeline[count].set_start(self.transients[count])
                self.timeline[count] = self.timeline[count].set_duration(self.transient_length)
                # self.timeline[count] = self.timeline[count].fadeout(self.timeline[count], self.transient_length/4, final_color=0)
            # timeline = concatenate_videoclips(self.timeline).set_duration(self.duration)
            timeline = CompositeVideoClip(self.timeline, size=(1000, 1000)).set_position("center")
        return timeline

    def export_timeline(self):
        print("About to export ...")
        if self.layers > 1:
            finalComposition = CompositeVideoClip(self.tracks)
        else:
            if len(self.tracks) == 0:
                print("Error encoutered, no videos in timeline.")
                return
            else:
                finalComposition = self.tracks[0]
        for t in self.tracks:
            t.close()
        finalComposition = finalComposition.set_audio(self.audio)
        cores = multiprocessing.cpu_count()-1
        #hardcodded the FPS for now
        finalComposition.write_videofile("test.mp4", fps=20, codec='libx264', preset='ultrafast', threads=cores,
                                         # audio_codec='aac',
                                         # audio_fps=48000,
                                         audio=True,
                                         write_logfile=False)
        finalComposition.close()



    def pad_video(self):
        sequence_length = self.get_duration()
        elapsed_time = 0
        i = 0

        while self.finalComposition.duration <= sequence_length & i <= len(self.timeline):
            if elapsed_time + self.timeline[i].duration <= sequence_length:
                elapsed_time += self.timeline[i].duration
            else:
                # call a method to create a small piece to fit the gap
                time_left = sequence_length - elapsed_time
                self.timeline[i]

            i += 1
        return True