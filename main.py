import tkinter as tk
from tkinter import filedialog
from timeline_manager import TimelineManager
import re
import sys
import queue
import signal
import threading
from tkinter import ttk, VERTICAL, HORIZONTAL, YES, NO
import gc

video_loc = []
audio_loc = None
t_manager = None
accuracy = 5
chunk_weight = 1
layers = 1
transient = False
transient_w = 5
debug = False
pause_scroll = False
load = False
save = False
transparent = False


audio_loc = 'media/thepathtoyourself_keyCmin_131bpm.wav'
# audio_loc = 'media/Nick Mira - NOLA 140 BPM.wav'
# video_path0 = 'media/video.mp4'
# video_path1 = 'media/long.mkv'
video_path2 = 'media/surf.mp4'
video_path1 = 'media/Behind_the_Scenes.mov'
video_path0 = 'media/Green_Screen_test.mp4'
# video_path1 = 'media/red.mp4'
# video_path2 = 'media/green.mp4'
# video_path3 = 'media/blue.mp4'
video_loc = []
# video_loc = [video_path2, video_path0]


class PrintLogger:  # create file like object
    global pause_scroll

    def __init__(self, textbox, error_format):  # pass reference to text widget
        self.textbox = textbox  # keep ref
        self.error_format = error_format

    def write(self, text):
        if self.error_format:
            self.textbox.insert(tk.END, "\n")  # newline for error
        self.textbox.insert(tk.END, text)  # write text to textbox
        if not pause_scroll:
            self.textbox.see(tk.END)  # scroll the output

    def flush(self):  # needed for file like object
        pass


class ConsoleUi:
    def __init__(self, frame):
        self.frame = frame
        self.text_box = tk.Text(self.frame, wrap='word', height=11, width=50)
        self.text_box.grid(column=0, row=0, columnspan=2, sticky='NSWE', padx=5, pady=5)
        sys.stdout = PrintLogger(self.text_box, False)
        sys.stderr = PrintLogger(self.text_box, True)


class FormUi:
    def __init__(self, frame):
        self.frame = frame

        global chunk_w, layer_n, accur, transient_w_entry, transient_box, debug_box, load_box, save_box, transparent_box

        tk.Label(self.frame, text="Chunk length weighting:").pack(side=tk.TOP, anchor=tk.W, expand=YES)
        chunk_w = tk.Entry(self.frame)
        chunk_w.insert(tk.END, '8')
        chunk_w.pack(side=tk.TOP, anchor=tk.W, expand=tk.YES)

        tk.Label(self.frame, text="Number of chunk subsamples within chunk:").pack(side=tk.TOP, anchor=tk.W, expand=YES)
        accur = tk.Entry(self.frame)
        accur.insert(tk.END, '5')
        accur.pack(side=tk.TOP, anchor=tk.W, expand=tk.YES)

        tk.Label(self.frame, ).pack(side=tk.TOP, anchor=tk.W, expand=YES)
        tk.Label(self.frame, text="Number of layers/video tracks:").pack(side=tk.TOP, anchor=tk.W, expand=YES)
        layer_n = tk.Entry(self.frame)
        layer_n.insert(tk.END, '1')
        layer_n.pack(side=tk.TOP, anchor=tk.W, expand=tk.YES)

        transparent_box = ttk.Checkbutton(self.frame, text="Enable Transparency On Video Layers")
        transparent_box.pack(side=tk.TOP, anchor=tk.W, expand=tk.YES)


        tk.Label(self.frame, ).pack(side=tk.TOP, anchor=tk.W, expand=YES)
        transient_box = ttk.Checkbutton(self.frame, text="Enable Transient Edit")
        transient_box.pack(side=tk.TOP, anchor=tk.W, expand=YES)

        tk.Label(self.frame, text="Transient Length %").pack(side=tk.TOP, anchor=tk.W, expand=YES)
        transient_w_entry = tk.Entry(self.frame)
        transient_w_entry.insert(tk.END, '5')
        transient_w_entry.pack(side=tk.TOP, anchor=tk.W, expand=tk.YES)

        tk.Label(self.frame, ).pack(side=tk.TOP, anchor=tk.W, expand=YES)
        debug_box = ttk.Checkbutton(self.frame, text="Enable Debug Text")
        debug_box.pack(side=tk.TOP, anchor=tk.W, expand=YES)

        tk.Label(self.frame, ).pack(side=tk.TOP, anchor=tk.W, expand=YES)
        load_box = ttk.Checkbutton(self.frame, text="Enable Loading Video Analysis Files")
        load_box.pack(side=tk.TOP, anchor=tk.W, expand=YES)

        save_box = ttk.Checkbutton(self.frame, text="Enable Saving Video Analysis Files")
        save_box.pack(side=tk.TOP, anchor=tk.W, expand=YES)

class MediaUi:
    def select_audio(self):
        global audio_loc
        audio_loc = filedialog.askopenfilename()

    def select_video(self):
        global video_loc, vid
        video_loc = list(video_loc)
        video_loc.append(filedialog.askopenfilename())
        vid.insert(len(video_loc), video_loc[-1])

    def remove_video(self):
        global video_loc, vid
        video_loc = list(video_loc)
        temp = str(vid.curselection())
        temp = re.sub("[^0-9]", "", temp)
        if temp != "":
            vid.delete(temp)
            del video_loc[int(temp)]

    def pause_scroll(self):
        global pause_scroll
        pause_scroll = not pause_scroll

    def run_thread(self):
        ThreadedTask(self.queue).start()

    def __init__(self, frame):
        global vid
        self.frame = frame
        self.queue = queue.Queue()
        fm4 = tk.Frame(self.frame)

        aud = tk.Button(fm4, text="Load Audio", command=self.select_audio)
        aud.pack(side=tk.RIGHT, expand=YES)
        pause = tk.Button(fm4, text="Pause Console Scroll", command=self.pause_scroll)
        pause.pack(side=tk.RIGHT, expand=10)
        run = tk.Button(fm4, text="RUN", command=self.run_thread)
        run.pack(side=tk.RIGHT, expand=YES)

        video_lbl = tk.Label(self.frame, text="Video Files").pack(side=tk.LEFT, fill=tk.BOTH, expand=NO)
        add = tk.Button(self.frame, text="+", command=self.select_video)
        add.pack(side=tk.LEFT, expand=NO)
        rem = tk.Button(self.frame, text="-", command=self.remove_video)
        rem.pack(side=tk.LEFT, expand=NO)
        fm4.pack(side=tk.BOTTOM, expand=NO)
        vid = tk.Listbox(self.frame, selectmode="MULTIPLE")
        vid.pack(side=tk.BOTTOM, fill=tk.BOTH, anchor=tk.W, expand=YES)


class App:
    def __init__(self, root):
        self.root = root
        root.title("Automated music video editor")
        root.state("zoomed")
        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)
        # Create the panes and frames
        vertical_pane = ttk.PanedWindow(self.root, orient=VERTICAL)
        vertical_pane.grid(row=0, column=0, sticky="nsew")
        horizontal_pane = ttk.PanedWindow(vertical_pane, orient=HORIZONTAL)

        vertical_pane.add(horizontal_pane)
        settings_frame = ttk.Labelframe(horizontal_pane, text="Settings")
        settings_frame.columnconfigure(1, weight=1)
        horizontal_pane.add(settings_frame, weight=1)

        console_frame = ttk.Labelframe(vertical_pane, text="CONSOLE")
        vertical_pane.add(console_frame, weight=1)
        console_frame.columnconfigure(0, weight=1)
        console_frame.rowconfigure(0, weight=1)

        media_frame = ttk.Labelframe(horizontal_pane, text="Media")
        horizontal_pane.add(media_frame, weight=1)
        # Initialize all frames
        self.settings = FormUi(settings_frame)
        self.media = MediaUi(media_frame)
        self.console = ConsoleUi(console_frame)
        self.root.protocol('WM_DELETE_WINDOW', self.quit)
        self.root.bind('<Control-q>', self.quit)
        signal.signal(signal.SIGINT, self.quit)

    def quit(self, *args):
        self.root.destroy()


class ThreadedTask(threading.Thread):
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):
        print("########################################################")
        print("# EDITING STARTING")
        print("########################################################")
        global t_manager, chunk_w, layer_n, accur, transient_w_entry, transient_box, debug_box, transient, debug, video_loc, load_box, save_box, transparent_box
        # double check if everything is configured
        temp_str = [str(chunk_w.get()), str(layer_n.get()), str(accur.get()), str(transient_w_entry.get())]
        try:
            transient = transient_box.state().index("selected")
            transient = True
        except ValueError:
            transient = False
        try:
            debug = debug_box.state().index("selected")
            debug = True
        except ValueError:
            debug = False
        try:
            load_box.state().index("selected")
            load = True
        except ValueError:
            load = False
        try:
            save_box.state().index("selected")
            save = True
        except ValueError:
            save = False
        try:
            transparent_box.state().index("selected")
            transparent = True
        except ValueError:
            transparent = False

        for t in temp_str:
            temp = re.sub("[^0-9]", "", t)
            if temp == "":
                print("Unconfigured settings")
                return
        if audio_loc == None or len(video_loc) == 0 or audio_loc == "":
            print("Media files not supplied")
            return
        # make sur it's a list and not a set and if there are no empty values
        video_loc = set(video_loc)
        video_loc = [i for i in video_loc if i]
        temp_str = [int(i) for i in temp_str]
        chunk_weight, layer_number, accuracy_number, transient_w = temp_str

        if transient:
            layer_number += 1
        # Call video analysis and start organising chunks
        t_manager = TimelineManager(video_loc, audio_loc, chunk_weight=chunk_weight, layers=layer_number, debug=debug, transient=transient, load=load, save=save, transparent=transparent)
        # videos get preprocessed and divided
        # then analysed one by one in parallel and returned to chunk manager
        t_manager.get_chunks(accuracy=accuracy_number, transient_w=transient_w)
        # Export the video for full length of the song
        t_manager.export_timeline()
        print("Edit finished, video file is located in the root of the application.")
        print("\n")
        print(gc.get_stats())

def main():
    root = tk.Tk()
    app = App(root)
    app.root.mainloop()


if __name__ == '__main__':
    main()
