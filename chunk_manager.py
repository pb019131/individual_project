# import moviepy
# from moviepy.video.VideoClip import ImageClip, VideoClip
# from moviepy.video.compositing.CompositeVideoClip import CompositeVideoClip
# from moviepy.video.compositing.concatenate import concatenate_videoclips
from sklearn.preprocessing import StandardScaler

from video_analyser import VideoAnalyser
from moviepy.video.io.VideoFileClip import VideoFileClip
from random import randint
import similaritymeasures
import numpy as np
import csv
import pandas as pd
from moviepy.video.tools.segmenting import findObjects


def load_clip(video_loc):
    return VideoFileClip(filename=video_loc, audio=False)


class ChunkManager:
    # variables
    duration = 0
    chunk_number = 0
    chunk_length = 0
    v_analyser = None
    videos = []
    audio_chunks = []
    results = []
    starts = []
    audio_debug_text = []

    def __init__(self, a_analyser, duration, chunk_number, chunk_length, video_loc):
        self.a_analyser = a_analyser
        self.duration = duration
        self.chunk_length = chunk_length
        self.chunk_number = chunk_number
        self.video_loc = video_loc
        self.videos = list(map(load_clip, video_loc))
        self.v_analyser = VideoAnalyser(video_loc)
        self.starts = []
        self.video_chunks = []
        self.audio_transient_chunks = []
        self.transient_best_fit = []
        self.transient = False
        self.last_chunk = None
        self.subsamples = 3
        self.files_processed = []

    # can run this in parallel for different files to speed up
    def divide_video_chunks(self, accuracy):
        self.subsamples = accuracy
        for i in range(len(self.videos)):
            if i not in self.files_processed:
                self.divide_file(i)
                self.files_processed.append(i)

    def get_audio_chunks(self, accuracy):
        print("Getting audio chunks ... ")
        self.audio_chunks = self.a_analyser.analyse_chunks(self.chunk_length, accuracy)

    def get_audio_transient_chunks(self, accuracy):
        self.transient = True
        print("Getting transients ... ")
        self.audio_transient_chunks = self.a_analyser.analyse_transient_chunks(self.audio_chunks, accuracy)

    # do preprocessing and file loading here
    def divide_file(self, fileNum):
        file = self.videos[fileNum]
        # file = self.videos[fileNum].subclip(900, 1150)
        fps = file.fps
        # file = file.resize(0.25)
        file = file.resize(height=480)
        print("Dividing file:", file.filename, " ... ")
        print("Estimated chunks in file:", file.duration/self.chunk_length, " ... ")
        print("This might take a while depending on legth of files and chunk weighting ...")
        start = 0
        order = 0
        duration = file.duration
        step = self.chunk_length / self.subsamples
        while (start + self.chunk_length) <= duration:
            frames = [file.get_frame(t=start+(i*step)) for i in range(self.subsamples)]
            self.starts.append([start, fileNum, frames, fps, order])
            start = start+self.chunk_length
            order += 1
        file.close()
        self.videos[fileNum].close()

    # gets called by multiple threads to process in parallel
    def prepare_video_chunk(self, data):
        start, file, frames, fps, order = data
        end = start + self.chunk_length
        chunk = self.v_analyser.analyze_frames(file, order, frames, start, end, fps)
        self.video_chunks.append(chunk)
        update_msg = "Chunk number: " + str(len(self.video_chunks)) + "\n"
        print(update_msg)

    def match_chunks(self, debug):
        best_fit = []
        # Scaling inputs
        video_curves = [o.curve for o in self.video_chunks]
        audio_curves = [o.curve for o in self.audio_chunks]
        # video_curves = StandardScaler().fit_transform(video_curves)
        # audio_curves = StandardScaler().fit_transform(audio_curves)

        count = 0
        print("Starting chunk matching ...")

        for x in audio_curves:
            print("Audio chunk: ", count+1, "/", len(audio_curves))
            self.audio_chunks[count].get_debug_text()
            self.audio_debug_text.append(self.audio_chunks[count].debug_txt)
            item = self.closest_match(x, video_curves)

            if item != self.last_chunk:
                best_fit.append(item)
                self.last_chunk = item
            else:
                random = randint(int(item-1), int(item + 10))
                if item + random < len(video_curves):
                    item = item + random
                else:
                    item = int(len(video_curves)-1)
                best_fit.append(item)
                self.last_chunk = item
            count = count + 1
        count = 0
        if self.transient:
            transient_curves = [o.curve for o in self.audio_transient_chunks]
            for y in transient_curves:
                print("Transient chunk: ", count+1, "/", len(transient_curves))
                item = self.closest_match(y, video_curves)
                if item >= len(video_curves):
                    item = len(video_curves) - 1
                self.transient_best_fit.append(item)
                count = count + 1
        if debug:
            # set debug text here
            for b in best_fit:
                try:
                    self.video_chunks[b].get_debug_text()
                except:
                    print("Debug text encountered error.")

        return best_fit

    def closest_match(self, value, list):
        out = []
        # Dynamic Time Warping distance
        for i in range(len(list)):
            dwt, d = similaritymeasures.dtw(value, list[i])
            if dwt < 0:
                dwt = 0
            out.append(dwt)
        minDistance = out.index(min(out))
        return minDistance

    # do CSV writing
    def save_chunk_data(self):
        output = []
        # need to set the name based on file
        for c in self.video_chunks:
            for i in range(len(c.curve)):
                output.append([self.videos[c.file].filename, c.order, i, c.start, c.end, c.fps, str(c.curve[i][0]), str(c.curve[i][1])])

        # group the output by filename
        output = np.asarray(output)
        output = output[output[:, 0].argsort()]
        filename = "analysis/" + output[0][0].rsplit('/', 1)[1] + ".csv"
        df = pd.DataFrame(output)
        grouped = df.groupby(by=0)

        for file in grouped:
            newFilename = "analysis/" + file[0].rsplit('/', 1)[1] + ".csv"
            if filename != newFilename:
                filename = newFilename
            csv_writter = csv.writer(open(filename, 'w'), delimiter=";", lineterminator='\n')
            # get dataframe from tuple and remove filename column
            file = file[1]
            file = file.drop(0, axis='columns')
            # group by chunks
            chunks = file.groupby(by=1)
            for chunk in chunks:
                # sort by subsamples
                subsamples = chunk[1].groupby(by=2)
                for item in subsamples:
                    item = item[1].values.tolist()
                    for x in item:
                        csv_writter.writerow(x)

    def load_chunk_data(self):
        files = self.video_loc
        fileNames = []
        # get file numbers for adding chunks
        for a in files:
            fileNames.append(a.rsplit('/', 1)[1])

        for f in files:
            # check if that name exists in media loaded
            try:
                fileName = f.rsplit('/', 1)[1]
                fileNum = fileNames.index(fileName)
                f = "analysis/" + f.rsplit('/', 1)[1] + ".csv"
                data = pd.read_csv(f, delimiter=";", header=None)
            except:
                print("No Analysis file found for video:" + fileName)
                return
            lines = len(data.index)
            data = data.groupby(0)
            chunks = data.ngroups
            subsamples = int(lines/chunks)
            added = 0
            for chunk in data:
                chunk = chunk[1]
                curve = []
                for i in range(subsamples):
                    curve.append([chunk[5][i+added], chunk[6][i+added]])
                # no data for colour or intensity
                bundle = [0, 0, curve]
                newChunk = self.v_analyser.load_chunk(file=fileNum, order=int(chunk[0][added]), frames=bundle, start=chunk[2][added], end=chunk[3][added], fps=chunk[4][added])
                self.video_chunks.append(newChunk)
                print("Loading chunk:", len(self.video_chunks))
                added = added + subsamples
            self.files_processed.append(fileNum)
        return
