from concurrent.futures import ProcessPoolExecutor, as_completed, ThreadPoolExecutor
import multiprocessing
import time


class Processing:
    def __init__(self, function, data):
        self.function = function
        self.data = data
        print("Initialising", __name__)

    def run(self):
        if __name__ == 'processing':
            start = time.perf_counter()
            print("Number of chunks in files: ", len(self.data))
            # using -1 core available to not lock up the system completely
            cores = multiprocessing.cpu_count()-1
            print("Starting Video chunk processing on ", cores, " CPU cores ...")
            with ThreadPoolExecutor(max_workers=cores) as executor:
                executor.map(self.function, self.data)

            finish = time.perf_counter()
            print(f'Finished processing chunks in {round(finish - start, 2)} second(s)')