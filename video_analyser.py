import math
import cv2 as cv
import numpy as np
from PIL import ImageStat
from PIL import Image
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import colorsys
import matplotlib
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt


class VideoChunk:
    def __init__(self, file, order, start, end, intensity, colour, curve, fps):
        self.file = file
        self.order = order
        self.start = start
        self.end = end
        self.intensity = intensity
        self.colour = colour
        self.curve = curve
        self.debug_txt = ""
        self.mask = []
        self.fps = fps

    def get_debug_text(self):
        intensity, colour = (np.mean(self.intensity), np.mean(self.colour))
        self.debug_txt = "Intensity: {:.3f}".format(intensity) + "\n" + "Colour: {:.3f}".format(colour) + "\n"

    def chroma_key(self, frame, hue):
        # use fps to interpolate the hue curve
        frame_HLS = cv.cvtColor(frame, cv.COLOR_RGB2HLS)
        low = hue-0.1
        high = hue+0.1
        if low < 0: low = 0
        if high > 1: high = 1

        low = np.array([int(low*179), 50, 50])
        high = np.array([int(high*179), 200, 255])
        # low_text = ("Keying out colour low:", low[0]*2, (low[1]+1*100/255)-1, (low[2]+1*100/255)-1)
        # high_text = ("Keying out colour high:", high[0]*2, (high[1]*100+1/255)-1, (high[2]+1*100/255)-1)
        # print(low_text + high_text)
        mask = cv.inRange(frame_HLS, low, high)
        mask = cv.GaussianBlur(mask, (5, 5), 0)
        mask = mask / 255
        mask = 1-mask
        # plt.imshow(frame)
        # plt.imshow(mask, cmap='gray')
        return mask


#################################################################################################################
# MAIN
#################################################################################################################
class VideoAnalyser:
    # variables
    video_files = []

    def __init__(self, video_loc):
        self.video_files = video_loc

    def analyze_frames(self, file, order, frames, start, end, fps):
        # iterate over the given number of subsamples
        data = []
        intensity = []
        colour = []

        for f in frames:
            data.append([self.brightness(f), self.colours(f)])
            intensity.append(self.brightness(f))
            colour.append(self.colours(f))

        data = np.asarray(data)
        data = StandardScaler().fit_transform(data)
        pca = PCA(2) #, svd_solver="full"
        pca = pca.fit(data)
        curve = pca.transform(data)
        colour = np.asarray(colour)

        numberOfFrames = int(np.abs(fps*(start - end)))+1
        # colour = interp1d(temp,framesNums, kind='cubic')

        size = len(frames)
        data = colour
        xloc = np.arange(size)
        newsize = numberOfFrames
        new_xloc = np.linspace(0, size, newsize)
        colour = np.interp(new_xloc, xloc, data)

        #################################################################
        # USE LATER
        #################################################################
        # video_period = find_video_period(timeline_chunk, tmin=.3)
        # print('Analyzed the video, found a period of %.02f seconds' % video_period)
        return VideoChunk(file, order, start, end, intensity, colour, curve, fps)


    def load_chunk(self, file, order, frames, start, end, fps):
        intensity, colour, curve = frames
        curve = np.asarray(curve)
        return VideoChunk(file, order, start, end, intensity, colour, curve, fps)

    # Perceived Brightness: intensity not related with colour
    def brightness(self, frame):
        im = Image.fromarray(frame)
        stat = ImageStat.Stat(im)
        r, g, b = stat.mean
        brightness = math.sqrt(0.241 * (r ** 2) + 0.691 * (g ** 2) + 0.068 * (b ** 2))
        maximumIntensity = 255
        brightness = float(brightness) / maximumIntensity
        return brightness

    def colours(self, frame):
        pixels = np.float32(frame.reshape(-1, 3))
        n_colors = 1
        criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 200, .1)
        flags = cv.KMEANS_RANDOM_CENTERS

        _, labels, palette = cv.kmeans(pixels, n_colors, None, criteria, 10, flags)
        _, counts = np.unique(labels, return_counts=True)

        total = counts.sum()
        hist = counts.tolist()
        hist /= total
        hist.tolist()
        hues = []
        for color in palette:
            r, g, b = color / 255
            dominantHLS = colorsys.rgb_to_hls(r, g, b)
            hues.append(dominantHLS[0])

        colors = sorted([(percent, color) for (percent, color) in zip(hist, hues)])
        colors = colors[::-1]

        coverage = 0.0
        hues = []
        for (percent, color) in colors:
            if coverage <= 0.5:
                coverage = coverage + percent
                hues.append(color)
            elif coverage == 0.0:
                hues.append(color)
                break

        # dominant = np.mean(hues)
        dominant = hues[0]
        return dominant
