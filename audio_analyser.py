import numpy as np
import librosa as lb
import matplotlib.pyplot as plt
# import librosa.display
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler


class AudioChunk:
    def __init__(self, start, end, volume, key, curve):
        self.start = start
        self.end = end
        self.volume = volume
        self.key = key
        self.curve = curve
        self.debug_txt = ""

    def get_debug_text(self):
        self.debug_txt = "Intensity: {:.3f}".format(self.volume) + "\n" + "Note: {:.0f}".format(int(self.key/12)) + "\n"


def curve(data):
    data = np.asarray(data)
    data = StandardScaler().fit_transform(data)
    pca = PCA(2) #, svd_solver="full"
    pca = pca.fit(data)
    curve = pca.transform(data)

    # plt.figure(figsize=(14, 6))
    # plt.plot(curve)
    # plt.title('Audio curve data')
    # plt.tight_layout()
    # plt.show()
    return curve


class AudioAnalyser:
    # variables
    BPM = 0
    key = ''
    duration = 0
    beat_frames = None
    chunk_number_of_frames = 0

    def __init__(self, audio_loc, duration):
        self.duration = duration
        print('Loading audio file ... ')
        self.y, self.sr = lb.load(audio_loc)
        self.global_parameters()
        self.subsamples = 3

    def global_parameters(self):
        N_FFT = 2048
        HOP_LENGTH = 64

        # Do HPSS
        print('Harmonic-percussive separation ... ')
        y_harmonic, y_percussive = lb.effects.hpss(self.y)

        # Construct onset envelope from percussive component
        print('Tracking beats on percussive component ... ')

        # Track the beats
        onset_env = lb.onset.onset_strength(y=self.y, sr=self.sr, hop_length=HOP_LENGTH, n_fft=N_FFT)
        tempo, beats = lb.beat.beat_track(onset_envelope=onset_env, sr=self.sr, hop_length=HOP_LENGTH, units='time')
        print('Estimated percussive tempo: {:.2f} beats per minute'.format(tempo))
        # tempo, beats = lb.beat.beat_track(y=y_percussive, sr=self.sr, hop_length=
        # print('Estimated PERC tempo: {:.2f} beats per minute'.format(tempo))
        # tempo, beats = lb.beat.beat_track(y=self.y, sr=self.sr, hop_length=HOP_LENGTH)
        # print('Estimated RAW tempo: {:.2f} BPM'.format(tempo))

        while True:
            if 80 <= tempo <= 200:
                break
            if tempo < 80:
                tempo = tempo * 2
                # beats = beats * 2
            if tempo > 200:
                tempo = tempo / 2
                # beats = beats / 2

        # need to round the tempo so the divisions are consistent
        self.BPM = round(tempo)
        print('Set: {:.2f} BPM'.format(self.BPM))

        # rerun transient search
        # tempo, beats = lb.beat.beat_track(onset_envelope=onset_env, trim=2,  bpm=self.BPM, sr=self.sr, hop_length=HOP_LENGTH, units='time')

        # convert the frame indices of beat events into timestamps
        self.beat_frames = beats
        # self.beat_frames = lb.frames_to_time(beats, sr=self.sr, hop_length=HOP_LENGTH)
        # self.beat_frames = lb.time_to_frames(beats, sr=self.sr, hop_length=HOP_LENGTH)

        #####################################################################
        S = np.abs(lb.stft(y_harmonic))
        # S = np.abs(lb.stft(self.y))
        pitches, magnitudes = lb.piptrack(S=S, sr=self.sr, threshold=1, ref=np.mean, n_fft=N_FFT, hop_length=HOP_LENGTH, fmin=30, fmax=1600)

        indexA = 0
        indexB = 0
        maxMagnitude = 0
        for i in range((len(magnitudes[:, 0]))):
            for j in range((len(magnitudes[0, :]))):
                if magnitudes[i, j] > maxMagnitude:
                    maxMagnitude = magnitudes[i, j]
                    indexA = i
                    indexB = j
                j += 1
            i += 1

        pitch = pitches[indexA, indexB]

        self.key = lb.hz_to_note(pitch, octave=False)
        print("Estimated Fundamental Note:", self.key)

    def get_params(self):
        return [self.BPM, self.key, self.beat_frames]

    def analyse_chunks(self, chunk_length, accuracy):
        self.subsamples = accuracy
        C = lb.feature.chroma_cqt(y=self.y, sr=self.sr)

        noteNum = C.shape[0]
        frameNum = C.shape[1]
        notes = []
        super_threshold_indices = C < np.mean(C)
        C[super_threshold_indices] = 0

        for i in range(frameNum):
            indexA = 0
            maxMagnitude = 0
            for j in range(noteNum):
                if C[j, i] > maxMagnitude:
                    maxMagnitude = C[j, i]
                    indexA = j
            notes.append(indexA)

        # Make a new figure
        # plt.figure(figsize=(14, 6))
        #
        # # Display the chromagram: the energy in each chromatic pitch class as a function of time
        # # To make sure that the colors span the full range of chroma values, set vmin and vmax
        # librosa.display.specshow(C, sr=self.sr, x_axis='time', y_axis='chroma', vmin=0, vmax=1)
        # plt.plot(notes)
        # plt.title('Chromagram')
        # plt.tight_layout()
        # plt.show()

        # dB = lb.core.perceptual_weighting(np.abs(lb.stft(self.y)))
        dB = lb.core.amplitude_to_db(np.abs(lb.stft(self.y)))
        # dB = sk.MinMaxScaler().fit_transform(dB)
        dB = np.percentile(dB, 90, axis=0)

        # shift note numbers to avoid division by 0
        notes = [(n+1) for n in notes]

        # how many frames a chunk is
        chunkFrames = int(round(chunk_length * len(dB[:])/self.duration))
        self.chunk_number_of_frames = chunkFrames
        chunks = []
        # subsample step
        step = int(self.chunk_number_of_frames / self.subsamples)

        try:
            for x in range(len(notes))[::self.chunk_number_of_frames]:
                data = []
                for y in range(self.subsamples):
                    try:
                        data.append([dB[x + (y * step)], notes[x + (y * step)]])
                    except:
                        data.append([0, notes[-1]])
                        print("Chunk out of range: dB-", (x + (y * step)), "Notes-", x + (y * step))
                chunks.append(AudioChunk(x, x+self.chunk_number_of_frames, dB[x], notes[x], curve(data)))
        except:
            print("Chunk out of range: dB-")
        return chunks

    def analyse_transient_chunks(self, audio_chunks, accuracy):
        HOP_LENGTH = 64
        self.subsamples = accuracy
        transient_chunks = []

        dB = lb.core.amplitude_to_db(np.abs(lb.stft(self.y)))
        dB = np.percentile(dB, 90, axis=0)

        C = lb.feature.chroma_cqt(y=self.y, sr=self.sr)
        noteNum = C.shape[0]
        frameNum = C.shape[1]
        notes = []
        super_threshold_indices = C < np.mean(C)
        C[super_threshold_indices] = 0

        for i in range(frameNum):
            indexA = 0
            maxMagnitude = 0
            for j in range(noteNum):
                if C[j, i] > maxMagnitude:
                    maxMagnitude = C[j, i]
                    indexA = j
            notes.append(indexA)

        chunkFrames = int(round(self.chunk_number_of_frames * len(audio_chunks) / self.duration))
        # subsample step
        step = int(chunkFrames / self.subsamples)
        if step == 0:
            step = 1
            self.subsamples = chunkFrames
            print("Transient subsample number higher than depth, setting step to 1")

        try:
            for x in self.beat_frames:
                # itterate over the chunk steps to find which chunk(s) contain the transient
                z = 0
                x = lb.time_to_frames(x, sr=self.sr, hop_length=HOP_LENGTH)

                z = int(x / chunkFrames)
                data = []
                for y in range(self.subsamples):
                    try:
                        data.append([dB[z + (y * step)], notes[z + (y * step)]])
                    except:
                        print("Chunk out of range: dB-", (z + (y * step)), "Notes-", z + (y * step))
                transient_chunks.append(AudioChunk(z, z+chunkFrames, dB[z], notes[z], curve(data)))
        except:
            print("Chunk error", self.beat_frames)

        return transient_chunks


# Set the frame parameters to be equivalent to the librosa defaults in the file's native sampling rate
        # frame_length = (2048 * self.sr) // 22050
        # hop_length = (512 * self.sr) // 22050
        # print("HOP AND FRAME: ", hop_length, frame_length)
# Stream the data, working on 128 frames at a time
        # file = 'media/audio.wav'
        # stream = lb.stream(file, block_length=chunkTime, frame_length=frame_length, hop_length=hop_length)
        #chromas = []
        # for y in stream:
        #     chroma_block = lb.feature.chroma_stft(y=self.y, sr=self.sr, n_fft=frame_length, hop_length=hop_length, center=False)
        #     chromas.append(chroma_block)
        #
        # npa = np.asarray(chromas, dtype=np.float32)
        # print("Shape of chroma", npa.shape)